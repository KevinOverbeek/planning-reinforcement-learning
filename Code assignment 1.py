# -*- coding: utf-8 -*-
"""
Purpose:
    Carrying out assignment 1 for the course "Planning and Reinforcement Learning"
    
Version:
    3 - Added optional assignments
              
Date:
    2019-04-29
    
Author:
    Kevin Overbeek, Joshua Touati & Thijs Tiedemann
"""

# In[Set working directory]

# Import required package
import os # analysis:ignore

# Set working directory, get paths where datasets are stored
if os.getlogin() == "Kevin Overbeek":
    os.chdir('D:/Kevin Overbeek/Documents/Studie/2018-19 BA/5.5 Planning and Reinforcement Learning/Assignment 1')

# In[Imports]

# Import standard python packages
import pandas as pd # analysis:ignore
import numpy as np # analysis:ignore
import matplotlib.pyplot as plt # analysis:ignore
import pulp # analysis:ignore
import time

# In[Global constants]

# Define size of grid
N_rows = 4
N_cols = 4

# Define different states
Env_start = (3,0)
Env_end = (0,3)
Env_wreck = (2,2)
Env_crack = ((1,1), (1,3), (2,3), (3,1), (3,2), (3,3))

# Define slipping probability
P_slip = 0.05

# Define actions
Actions = ('L', 'R', 'U', 'D')

# Define rewards
Reward_end = 100
Reward_wreck = 20
Reward_crack = -10

# Other model parameters
Gamma = 0.9
Runs = 20

# In[Environment functions]

"""
Purpose:
    Create the environment for the icy grid problem
    
Inputs:
    N_rows = Number of rows in our environment
    N_cols = Number of columns in our environment
    Env_end = Terminal state, associated with reward "Reward_end"
    Env_wreck = Intermediary state, associated with reward "Reward_wreck"
    Env_crack = Terminal state associated with (negative) reward "Reward_crack"
    Reward_end = Reward received when terminal state is reached
    Reward_wreck = Reward recieved when wreck is reached
    Reward_crack = Reward received when crack is reached
    
Outputs:
    Environment: Created environment according to the inputs
    Rewards_matrix: Dataframe with rewards
    
Version:
    2019-04-05: Added reward function
"""

def create_environment(N_rows, N_cols, Env_end, Env_wreck, Env_crack, Reward_end, Reward_wreck, Reward_crack):
    
    # Make empty environment, set all elements to 'I' (Ice)
    Environment = pd.DataFrame(np.zeros([N_rows,N_cols]))
    Rewards_matrix = Environment.copy()
    Environment = Environment.replace(0, 'I')
    
    # Place end position
    Environment.iloc[Env_end] = 'E'
    Rewards_matrix.iloc[Env_end] = Reward_end 
    
    # Place wreck position
    Environment.iloc[Env_wreck] = 'W'
    Rewards_matrix.iloc[Env_wreck] = Reward_wreck
    
    # Place all cracks
    for c in Env_crack:
        Environment.iloc[c] = 'C'
        Rewards_matrix.iloc[c] = Reward_crack

    return Environment, Rewards_matrix

"""
Purpose:
    Update position under a given environment
    
Inputs:
    Environment = Textual representation of the environment as made in create_environment
    Position_start = Current position (i.e. before action is taken)
    Selected_action = Action to be taken
    Actions = All posible actions that can be taken
    P_slip = Probability of slipping
    
Outputs:
    Probabilities = Matrix with transition probabilities
    
Version:
    2019-04-05: First version
"""

def perform_action(Environment, Position_start, Selected_action, Actions, P_slip):
    
    # Check if selected action is valid
    if Selected_action not in Actions:
        print("Invalid action selected, please choose from: " + str(Actions))
        return
    
    # Make empty matrix with transition probabilities
    Probabilities = np.zeros(Environment.shape)
    
    # Check if in terminal state
    if Environment.iloc[Position_start] in ('C', 'E'):
        Probabilities[Position_start] = 1
        return Probabilities
    
    # Determine current location and bounds
    Current_row = Position_start[0]
    Current_col = Position_start[1]
    LB_row = 0
    LB_col = 0
    UB_row = Environment.shape[0] - 1
    UB_col = Environment.shape[1] - 1
    
    # Determine location without slipping
    New_row = Current_row
    New_col = Current_col
    
    # Determine new location
    if Selected_action == 'L':
        New_col -= 1
    elif Selected_action == 'R':
        New_col += 1
    elif Selected_action == 'U':
        New_row -= 1
    elif Selected_action == 'D':
        New_row += 1
    
    # Force within bounds
    if New_col < LB_col:
        New_col = LB_col
    elif New_col > UB_col:
        New_col = UB_col
    
    if New_row < LB_row:
        New_row = LB_row
    elif New_row > UB_row:
        New_row = UB_row
    
    Probabilities[(New_row, New_col)] = 1 - P_slip
    
    # Determine location with slipping
    New_row_slip = Current_row
    New_col_slip = Current_col
    
    if Selected_action == 'L':
        while Environment.iloc[(New_row_slip, New_col_slip)] != 'C' and New_col_slip > LB_col:
            New_col_slip -= 1
    elif Selected_action == 'R':
        while Environment.iloc[(New_row_slip, New_col_slip)] != 'C' and New_col_slip < UB_col:
            New_col_slip += 1
    elif Selected_action == 'U':
        while Environment.iloc[(New_row_slip, New_col_slip)] != 'C' and New_row_slip > LB_row:
            New_row_slip -= 1
    elif Selected_action == 'D':
        while Environment.iloc[(New_row_slip, New_col_slip)] != 'C' and New_row_slip < UB_row:
            New_row_slip += 1
    
    Probabilities[(New_row_slip, New_col_slip)] += P_slip
    
    return Probabilities

# In[Policy functions]

"""
Purpose:
    Evaluation of a single policy
    
Inputs:
    Policy: Matrix with for every state the probabilities of picking a certain action
    Gamma: Discount rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    
Outputs:
    Vk1: Values for every state given the policy
    Qk: State-action values
        
Version:
    2019-04-10: Also returns Q values
"""

def evaluate_policy(Policy, Gamma, Environment, Rewards, P_slip):
    
    # Initialize V0 & flatten reward
    V0 = pd.DataFrame(np.zeros([1,Policy.shape[1]]), columns=Policy.columns.values)
    Rewards_flat = np.array(Rewards).flatten()
    
    # Determine terminal states, Coordinates and Actions
    Coordinates = Policy.columns.values
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]
    Actions = Policy.index.values
    
    # Initialize V & Q values
    Vk = V0.copy() + 1
    Vk1 = V0.copy()
    Qk = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    
    # Loop until convergence
    i = 0
    while not np.allclose(Vk, Vk1):
        i += 1 
        
        # Remember previous estimate 
        Vk = Vk1.copy()
        
        # For every coordinate determine value
        for c in Coordinates:
            
            # If terminal state, fix value to 0
            if c in Terminal:
                Vk1[c] = 0
                Qk[c] = 0
            
            # Else calculate reward and value of next state
            else:
                
                # Calculate expected value for every state-action (Q value)
                for a in Actions:
                    Probabilities = perform_action(Environment, c, a, Actions, P_slip)
                    Qk.loc[a,c] = np.dot(Probabilities.flatten(), (Rewards_flat + Gamma * Vk).T)
                
                # Calculate new Vk1
                Pi_sa = Policy[c]    
                Vk1[c] = np.dot(Pi_sa, Qk[c].T)
                
    # Print number of iterations needed                    
    print("Finished after " + str(i) + ' iterations')    
    return Vk1, Qk

"""
Purpose:
    Implement policy improvement algorithm
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction    
    method: Policy improvement method, choose from "Howard" or "Simple"
    
Outputs:
    Pi_optimal: Matrix with probabilities of taking each action in every state
    V: Values for every state choosing the optimal policy
    Q: State-action rewards ,
    Optimal_policy: Optimal deterministic policy for the agent
        
Version:
    2019-04-12: Added policy improvement method
"""

def improve_policy(Coordinates, Actions, Gamma, Environment, Rewards, P_slip, method):
    
    # Check chosen method
    if method not in ("Howard", "Simple"):
        print("Invalid improvement method, choose from 'Howard' or 'Simple'")
        return
    
    # Initialize random and empty policy
    Policy_k1 = pd.DataFrame(np.ones([len(Actions),len(Coordinates)]) / len(Actions), index=Actions, columns=Coordinates)
    Policy_k = Policy_k1.copy() * 0
    
    # Determine terminal states, Coordinates and Actions
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]
    
    # Keep improving / evaluating until best policy is reached
    while not np.allclose(Policy_k, Policy_k1):
        
        # Keep current policy
        Policy_k = Policy_k1.copy()
        
        # Evaluate current policy (k)
        Vk, Qk = evaluate_policy(Policy_k, Gamma, Environment, Rewards, P_slip)
        
        # Update policy based on chosen method
        if method == "Howard":
            
            # Update policy per state 
            for c in Coordinates:
                Policy_k1[c] = 0
                Policy_k1.loc[Qk[c].idxmax(), c] = 1
    
        elif method == "Simple":
            
            # Update first state which can be improved
            Improved = False
            i = 0
            while not Improved and i < len(Coordinates):
                
                # Retrieve coordinate
                c = Coordinates[i]
                
                # Retrieve old policy
                PolicyOld = Policy_k[c]
                
                # Calculate new policy
                PolicyNew = PolicyOld.copy() * 0
                PolicyNew[Qk[c].idxmax()] = 1
                
                # If difference, update
                if any(PolicyOld != PolicyNew):
                    Policy_k1[c] = PolicyNew
                    Improved = True
                
                # Update index for next run of while loop
                i += 1
    
    # Calculate optimal values
    Pi_optimal = Policy_k1
    V = Vk
    Q = Qk
    
    Optimal_policy = Qk.idxmax(axis=0)
    Optimal_policy.loc[Terminal] = '-'
    Optimal_policy = np.array(Optimal_policy).reshape(Environment.shape)
        
    return Pi_optimal, V, Q, Optimal_policy

"""
Purpose:
    Implementation of value iteration
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    
Outputs:
    Vk1: Values for every state choosing the optimal policy
    Qk1: State-action rewards ,
    Optimal: Optimal deterministic policy for the agent
    
Version:
    2019-04-10: First version
"""

def value_iteration(Coordinates, Actions, Gamma, Environment, Rewards, P_slip):
    
    # Initialize V0 & flatten reward
    V0 = pd.DataFrame(np.zeros([1,len(Coordinates)]), columns=Coordinates)
    Rewards_flat = np.array(Rewards).flatten()
    
    # Determine terminal states, Coordinates and Actions
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]
    
    # Initialize Q values
    Qk = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    Qk1 = Qk.copy() + 1
    
    # Initialize V values
    Vk = V0+1
    Vk1 = V0
    
    # Loop until convergence
    i = 0
    while not np.allclose(Vk, Vk1):
        i += 1
        
        # Remember previous estimate
        Vk = Vk1.copy()
        Qk = Qk1.copy()
        
        # For every coordinate determine value
        for c in Coordinates:
            
            # If terminal state, set Q values to 0
            if c in Terminal:
                Qk1[c] = 0
            
            # Else calculate new Q values
            else:
                
                for a in Actions:
                    Probabilities = perform_action(Environment, c, a, Actions, P_slip)
                    Qk1.loc[a,c] = np.dot(Probabilities.flatten(), (Rewards_flat + Gamma * Vk).T)

            # Set new V value to maximum of Q values            
            Vk1[c] = Qk1[c].max()
    
    print("Finished after " + str(i) + ' iterations')       
    
    # Determine optimal policy, set to '-' if terminal state, reshape
    Optimal = Qk1.idxmax(axis=0)
    Optimal.loc[Terminal] = '-'
    Optimal = np.array(Optimal).reshape(Environment.shape)
    
    return Vk1, Qk1, Optimal

# In[Linear programming functions]
    
"""
Purpose:
    Finding optimal policy by means of linear programming
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    
Outputs:
    V: PulP output of optimized variables
    
Version:
    2019-04-17: First version
"""

def LP_planning(Coordinates, Actions, Gamma, Environment, Rewards, P_slip):
    
    # Initialize V0 & flatten reward
    V0 = pd.DataFrame(np.zeros([1,len(Coordinates)]), columns=Coordinates)
    Rewards_flat = np.array(Rewards).flatten()
    
    # Determine terminal states, Coordinates and Actions
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]
    
    ##### Linear optimization part #####
    
    # Create the model
    model = pulp.LpProblem(name="Model", sense = pulp.LpMinimize)
    
    # Make pulp variable (adjustable)
    V = pulp.LpVariable.dicts("V", range(V0.shape[1]))

    # Add objective
    model += pulp.lpSum(V[i] for i in range(V0.shape[1]))

    # Add constraints for every state
    for i in range(V0.shape[1]):
        
        # State value = 0 in terminal state
        if Coordinates[i] in Terminal:
            model += V[i] == 0
            
        else:
            
            # Add constraints for non-terminal states
            for j in range(len(Actions)):
                
                # Use transition probabilities to determine expected reward / state-value
                Probabilities = perform_action(Environment, Coordinates[i], Actions[j], Actions, P_slip).flatten()
                model += V[i] >= pulp.lpSum(Gamma * Probabilities[k] * V[k] for k in range(V0.shape[1])) + pulp.lpSum(Probabilities[k] * Rewards_flat[k] for k in range(V0.shape[1]))
    
    # Solve model
    model.solve()
    print("Status: " + pulp.LpStatus[model.status])
       
    # Each of the variables is printed with it's resolved optimum value
    for v in model.variables():
        print(v.name + " = " + str(v.varValue))
    
    # Return V values
    V = model.variables()
    
    return V

# In[Main]

def main():
    
    ##### Q1: Creating environment #####
    Environment, Rewards = create_environment(N_rows, N_cols, Env_end, Env_wreck, Env_crack, Reward_end, Reward_wreck, Reward_crack)
    
    # Test action
    Probabilities = perform_action(Environment, (3,0), 'U', Actions, P_slip)
    print(Probabilities)
    
    ##### Q2: Random policy #####
    
    # Retrieve all coordinates of the grid
    Coordinates = ()
    for i in range(N_rows):
        for j in range(N_cols):
            Coordinates = (*Coordinates, (i,j))
            
    # Make a random policy
    Pi_random = pd.DataFrame(np.ones([len(Actions),(N_rows*N_cols)]) / len(Actions), index=Actions, columns=Coordinates)
    
    # Evaluate the random policy
    V_random, Qk_random = evaluate_policy(Pi_random, Gamma, Environment, Rewards, P_slip)
    print(V_random)
    
    ##### Q3: Value iteration #####
    
    # Call value iteration function, print optimal deterministic policy
    start = time.time()
    for i in range(Runs):
        V_VI, Q_VI, Optimal_VI = value_iteration(Coordinates, Actions, Gamma, Environment, Rewards, P_slip)
    end = time.time()
    print("Time needed to run algorithm: " + str((end - start) / 20))
    print(V_VI)
    print(Optimal_VI)
    
    ##### Q4: Policy improvement #####
    method = "Howard"
    start = time.time()
    for i in range(Runs):
        Pi_optimal, V_PolicyImp, Q_PolicyImp, Optimal_PolicyImp = improve_policy(Coordinates, Actions, Gamma, Environment, Rewards, P_slip, method)
    end = time.time()
    print("Time needed to run algorithm: " + str((end - start) / 20))
    print(Optimal_PolicyImp)    
    
    ##### Q5a: Simple policy improvement #####
    method= "Simple"
    start = time.time()
    for i in range(Runs):
        Pi_optimal, V_PolicyImp, Q_PolicyImp, Optimal_PolicyImp = improve_policy(Coordinates, Actions, Gamma, Environment, Rewards, P_slip, method)
    end = time.time()
    print("Time needed to run algorithm: " + str((end - start) / 20))
    print(Optimal_PolicyImp)
    
    ##### Q5b: Solve lower bound by linear programming #####
    start = time.time()
    for i in range(Runs):
        V_LP = LP_planning(Coordinates, Actions, Gamma, Environment, Rewards, P_slip) #analysis:ignore
    end = time.time()
    print("Time needed to run algorithm: " + str((end - start) / 20))
    
### start main
if __name__ == "__main__":
    main()

