# -*- coding: utf-8 -*-
"""
Purpose:
    Carrying out assignment 2 for the course "Planning and Reinforcement Learning"
    
Version:
    1 - First version based on assignment 1
              
Date:
    2019-04-29
    
Author:
    Kevin Overbeek, Joshua Touati & Thijs Tiedemann
"""

# In[Imports]

# Import standard python packages
import pandas as pd # analysis:ignore
import numpy as np # analysis:ignore
import matplotlib.pyplot as plt # analysis:ignore
import pulp # analysis:ignore
import random as rand
import bisect

# In[Global constants]

### Environment parameters ###

# Define size of grid
N_rows = 4
N_cols = 4

# Define different states
Env_start = (3,0)
Env_end = (0,3)
Env_wreck = (2,2)
Env_crack = ((1,1), (1,3), (2,3), (3,1), (3,2), (3,3))
Initial_state = Env_start

# Define slipping probability
P_slip = 0.05

# Define actions
Actions = ('L', 'R', 'U', 'D')

# Define rewards
Reward_end = 100
Reward_wreck = 20
Reward_crack = -10

### Model parameters ###

# Discount and learning rate
Gamma = 0.9
Alpha = 0.02

# Exploration parameters
Epsilon = 0.05
Tau = 1

# Number of simulations
N_episodes = 2500
Eval_interval = 50
Eval_episodes = 500

# Experience replay parameters
T_ER = 100 # Number of steps before updating
N_ER = 1 # Number of updates per T

# Eligibility traces parameter
Lambda = 0.5

# Prioritized sweeping parameter
N_PS = 10

# In[Environment functions]

"""
Purpose:
    Create the environment for the icy grid problem
    
Inputs:
    N_rows = Number of rows in our environment
    N_cols = Number of columns in our environment
    Env_end = Terminal state, associated with reward "Reward_end"
    Env_wreck = Intermediary state, associated with reward "Reward_wreck"
    Env_crack = Terminal state associated with (negative) reward "Reward_crack"
    Reward_end = Reward received when terminal state is reached
    Reward_wreck = Reward recieved when wreck is reached
    Reward_crack = Reward received when crack is reached
    
Outputs:
    Environment: Created environment according to the inputs
    Rewards_matrix: Dataframe with rewards
    
Version:
    2019-04-05: Added reward function
"""

def create_environment(N_rows, N_cols, Env_end, Env_wreck, Env_crack, Reward_end, Reward_wreck, Reward_crack):
    
    # Make empty environment, set all elements to 'I' (Ice)
    Environment = pd.DataFrame(np.zeros([N_rows,N_cols]))
    Rewards_matrix = Environment.copy()
    Environment = Environment.replace(0, 'I')
    
    # Place end position
    Environment.iloc[Env_end] = 'E'
    Rewards_matrix.iloc[Env_end] = Reward_end 
    
    # Place wreck position
    Environment.iloc[Env_wreck] = 'W'
    Rewards_matrix.iloc[Env_wreck] = Reward_wreck
    
    # Place all cracks
    for c in Env_crack:
        Environment.iloc[c] = 'C'
        Rewards_matrix.iloc[c] = Reward_crack

    return Environment, Rewards_matrix

"""
Purpose:
    Update position under a given environment
    
Inputs:
    Environment = Textual representation of the environment as made in create_environment
    Position_start = Current position (i.e. before action is taken)
    Selected_action = Action to be taken
    Actions = All posible actions that can be taken
    P_slip = Probability of slipping
    
Outputs:
    Probabilities = Matrix with transition probabilities
    
Version:
    2019-04-05: First version
"""

def perform_action(Environment, Position_start, Selected_action, Actions, P_slip):
    
    # Check if selected action is valid
    if Selected_action not in Actions:
        print("Invalid action selected, please choose from: " + str(Actions))
        return
    
    # Make empty matrix with transition probabilities
    Probabilities = np.zeros(Environment.shape)
    
    # Check if in terminal state
    if Environment.iloc[Position_start] in ('C', 'E'):
        Probabilities[Position_start] = 1
        return Probabilities
    
    # Determine current location and bounds
    Current_row = Position_start[0]
    Current_col = Position_start[1]
    LB_row = 0
    LB_col = 0
    UB_row = Environment.shape[0] - 1
    UB_col = Environment.shape[1] - 1
    
    # Determine location without slipping
    New_row = Current_row
    New_col = Current_col
    
    # Determine new location
    if Selected_action == 'L':
        New_col -= 1
    elif Selected_action == 'R':
        New_col += 1
    elif Selected_action == 'U':
        New_row -= 1
    elif Selected_action == 'D':
        New_row += 1
    
    # Force within bounds
    if New_col < LB_col:
        New_col = LB_col
    elif New_col > UB_col:
        New_col = UB_col
    
    if New_row < LB_row:
        New_row = LB_row
    elif New_row > UB_row:
        New_row = UB_row
    
    Probabilities[(New_row, New_col)] = 1 - P_slip
    
    # Determine location with slipping
    New_row_slip = Current_row
    New_col_slip = Current_col
    
    if Selected_action == 'L':
        while Environment.iloc[(New_row_slip, New_col_slip)] != 'C' and New_col_slip > LB_col:
            New_col_slip -= 1
    elif Selected_action == 'R':
        while Environment.iloc[(New_row_slip, New_col_slip)] != 'C' and New_col_slip < UB_col:
            New_col_slip += 1
    elif Selected_action == 'U':
        while Environment.iloc[(New_row_slip, New_col_slip)] != 'C' and New_row_slip > LB_row:
            New_row_slip -= 1
    elif Selected_action == 'D':
        while Environment.iloc[(New_row_slip, New_col_slip)] != 'C' and New_row_slip < UB_row:
            New_row_slip += 1
    
    Probabilities[(New_row_slip, New_col_slip)] += P_slip
    
    return Probabilities

# In[Policy functions]

"""
Purpose:
    Evaluation of a single policy
    
Inputs:
    Policy: Matrix with for every state the probabilities of picking a certain action
    Gamma: Discount rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    
Outputs:
    Vk1: Values for every state given the policy
    Qk: State-action values
        
Version:
    2019-04-10: Also returns Q values
"""

def evaluate_policy(Policy, Gamma, Environment, Rewards, P_slip):
    
    # Initialize V0 & flatten reward
    V0 = pd.DataFrame(np.zeros([1,Policy.shape[1]]), columns=Policy.columns.values)
    Rewards_flat = np.array(Rewards).flatten()
    
    # Determine terminal states, Coordinates and Actions
    Coordinates = Policy.columns.values
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]
    Actions = Policy.index.values
    
    # Initialize V & Q values
    Vk = V0.copy() + 1
    Vk1 = V0.copy()
    Qk = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    
    # Loop until convergence
    i = 0
    while not np.allclose(Vk, Vk1):
        i += 1 
        
        # Remember previous estimate 
        Vk = Vk1.copy()
        
        # For every coordinate determine value
        for c in Coordinates:
            
            # If terminal state, fix value to 0
            if c in Terminal:
                Vk1[c] = 0
                Qk[c] = 0
            
            # Else calculate reward and value of next state
            else:
                
                # Calculate expected value for every state-action (Q value)
                for a in Actions:
                    Probabilities = perform_action(Environment, c, a, Actions, P_slip)
                    Qk.loc[a,c] = np.dot(Probabilities.flatten(), (Rewards_flat + Gamma * Vk).T)
                
                # Calculate new Vk1
                Pi_sa = Policy[c]    
                Vk1[c] = np.dot(Pi_sa, Qk[c].T)
                
    # Print number of iterations needed                    
    print("Finished after " + str(i) + ' iterations')    
    return Vk1, Qk

"""
Purpose:
    Implement policy improvement algorithm
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction    
    method: Policy improvement method, choose from "Howard" or "Simple"
    
Outputs:
    Pi_optimal: Matrix with probabilities of taking each action in every state
    V: Values for every state choosing the optimal policy
    Q: State-action rewards ,
    Optimal_policy: Optimal deterministic policy for the agent
        
Version:
    2019-04-12: Added policy improvement method
"""

def improve_policy(Coordinates, Actions, Gamma, Environment, Rewards, P_slip, method):
    
    # Check chosen method
    if method not in ("Howard", "Simple"):
        print("Invalid improvement method, choose from 'Howard' or 'Simple'")
        return
    
    # Initialize random and empty policy
    Policy_k1 = pd.DataFrame(np.ones([len(Actions),len(Coordinates)]) / len(Actions), index=Actions, columns=Coordinates)
    Policy_k = Policy_k1.copy() * 0
    
    # Determine terminal states, Coordinates and Actions
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]
    
    # Keep improving / evaluating until best policy is reached
    while not np.allclose(Policy_k, Policy_k1):
        
        # Keep current policy
        Policy_k = Policy_k1.copy()
        
        # Evaluate current policy (k)
        Vk, Qk = evaluate_policy(Policy_k, Gamma, Environment, Rewards, P_slip)
        
        # Update policy based on chosen method
        if method == "Howard":
            
            # Update policy per state 
            for c in Coordinates:
                Policy_k1[c] = 0
                Policy_k1.loc[Qk[c].idxmax(), c] = 1
    
        elif method == "Simple":
            
            # Update first state which can be improved
            Improved = False
            i = 0
            while not Improved and i < len(Coordinates):
                
                # Retrieve coordinate
                c = Coordinates[i]
                
                # Retrieve old policy
                PolicyOld = Policy_k[c]
                
                # Calculate new policy
                PolicyNew = PolicyOld.copy() * 0
                PolicyNew[Qk[c].idxmax()] = 1
                
                # If difference, update
                if any(PolicyOld != PolicyNew):
                    Policy_k1[c] = PolicyNew
                    Improved = True
                
                # Update index for next run of while loop
                i += 1
    
    # Calculate optimal values
    Pi_optimal = Policy_k1
    V = Vk
    Q = Qk
    
    Optimal_policy = Qk.idxmax(axis=0)
    Optimal_policy.loc[Terminal] = '-'
    Optimal_policy = np.array(Optimal_policy).reshape(Environment.shape)
        
    return Pi_optimal, V, Q, Optimal_policy

"""
Purpose:
    Implementation of value iteration
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    
Outputs:
    Vk1: Values for every state choosing the optimal policy
    Qk1: State-action rewards ,
    Optimal: Optimal deterministic policy for the agent
    
Version:
    2019-04-10: First version
"""

def value_iteration(Coordinates, Actions, Gamma, Environment, Rewards, P_slip):
    
    # Initialize V0 & flatten reward
    V0 = pd.DataFrame(np.zeros([1,len(Coordinates)]), columns=Coordinates)
    Rewards_flat = np.array(Rewards).flatten()
    
    # Determine terminal states, Coordinates and Actions
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]
    
    # Initialize Q values
    Qk = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    Qk1 = Qk.copy() + 1
    
    # Initialize V values
    Vk = V0+1
    Vk1 = V0
    
    # Loop until convergence
    i = 0
    while not np.allclose(Vk, Vk1):
        i += 1
        
        # Remember previous estimate
        Vk = Vk1.copy()
        Qk = Qk1.copy()
        
        # For every coordinate determine value
        for c in Coordinates:
            
            # If terminal state, set Q values to 0
            if c in Terminal:
                Qk1[c] = 0
            
            # Else calculate new Q values
            else:
                
                for a in Actions:
                    Probabilities = perform_action(Environment, c, a, Actions, P_slip)
                    Qk1.loc[a,c] = np.dot(Probabilities.flatten(), (Rewards_flat + Gamma * Vk).T)

            # Set new V value to maximum of Q values            
            Vk1[c] = Qk1[c].max()
    
    print("Finished after " + str(i) + ' iterations')       
    
    # Determine optimal policy, set to '-' if terminal state, reshape
    Optimal = Qk1.idxmax(axis=0)
    Optimal.loc[Terminal] = '-'
    Optimal = np.array(Optimal).reshape(Environment.shape)
    
    return Vk1, Qk1, Optimal

# In[Q-learning & SARSA functions]

"""
Purpose:
    Action selection according to epsilon-greedy or softmax
    
Inputs:
    Actions: Tuple with all actions the agent can take
    Epsilon: Probability of selecting a random action
    Q: the current Q-values
    Method: The exploration mechanism
    
Outputs:
    Action: the action to take
    
Version:
    2019-04-26: First version
"""

def select_action(Actions, Epsilon, Tau, Q, Method):
    
    # Determine probabilities of each action per method
    
    if Method == "Epsilon_greedy":
        Prob = pd.Series(np.ones(len(Q)), index = Actions) * Epsilon / len(Q)
        Prob.loc[Q.idxmax()] += (1 - Epsilon)
    elif Method == "Softmax":
        Prob = np.exp(Q/Tau)
        Prob = Prob / Prob.sum()
    
    # Determine cumulative distribution        
    cdf = []
    Total = 0
    for a in range(len(Actions)-1):
        Total += Prob.iloc[a]
        cdf.append(Total)
    
    # Force last entry to be 1
    cdf.append(1)
    
    # Select action based on cdf
    x = rand.random()
    idx = bisect.bisect(cdf, x)
    
    # Select action based on index
    Action = Actions[idx]
    
    return Action

"""
Purpose:
    Simulate effect of action
    
Inputs:
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    s: Current state
    a: Selected action
    P_slip: Probability of slipping and sliding to last state in chosen direction
    
Outputs:
    s_next: Simulated next state
    r: Reward obtained by action
    
Version:
    2019-04-26: First version
"""

def simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip):
    
    # Call transition probabilities function, flatten into vector
    Transition = perform_action(Environment, s, a, Actions, P_slip)
    Transition = Transition.flatten()
    
    # Determine cumulative distribution
    cdf = []
    Total = 0
    for i in range(len(Transition)-1):
        Total += Transition[i]
        cdf.append(Total)
    
    # Force last entry to be 1
    cdf.append(1)    
    
    # Select next state based on cdf
    x = rand.random()
    idx = bisect.bisect(cdf, x)

    # Retrieve next state and reward based on index
    s_next = Coordinates[idx]
    r = np.array(Rewards).flatten()[idx]
    
    return s_next, r

"""
Purpose:
    Evaluate policy after every n timesteps
"""

def Eval_policy(Coordinates, Actions, Gamma, Environment, Rewards, Q, Terminal, Episodes, Initial, Optimal, P_slip):
    
    # Initialize reward variables
    Reward = []
    episode_reward = 0
    i = 0
    
    for e in range(Episodes):
        
        s = Initial
        
        n = 0
        while s not in Terminal and n <= 100:
            
            # Select greedy action
            a = Optimal.loc[s]
            
            # Take action a, and observe reward, r, and next state, s'
            (s_next, r) = simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip)
            
            # Add obtained reward to episode reward
            episode_reward += r * Gamma**i
                        
            # The current state becomes the next state
            s = s_next
            
            # Add one to the discount exponent
            i += 1
            n += 1
            
        # append reward obtained during episode & forget episode reward
        Reward.append(episode_reward)
        episode_reward = 0
        i = 0
        
    Value = np.mean(Reward)
    
    return Value
    

"""
Purpose:
    Implementation of Q-Learning
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Alpha: Learning rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    Episodes: Number of episodes to run algorithm
    Method: Exploration method
    
Outputs:
    Q: State-action values
    Optimal: Optimal deterministic policy for the agent
    Cumulative_reward: list of cumulative rewards obtained for each episode 
    
Version:
    2019-04-26: First version
"""

def Q_learning(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Environment, Rewards, P_slip, Episodes, Initial_state, Method, Eval_interval, Eval_episodes):
    
    # Initialize Q values & flatten rewards
    Q = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    
    # Determine terminal states
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]

    # Initialize non-terminal states
    for c in Coordinates:
        if c not in Terminal:
            Q[c] += Rewards.max().max()
    
    # Initialize cumulative reward & reward per episode                
    Reward = []
    Forward_reward = []
    episode_reward = 0
    i = 0

    for e in range(Episodes):
        
        # Initialize s (initial state)
        s = Initial_state
        
        while s not in Terminal:
            
            # Select action based on epsilon-greedy
            a = select_action(Actions, Epsilon, Tau, Q[s], Method)
            
            # Take action a, and observe reward, r, and next state, s'
            (s_next, r) = simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip)
            
            # Add obtained reward to episode reward
            episode_reward += r * Gamma**i
            
            # Update Q
            Q.loc[a,s] += Alpha * (r + (Gamma * Q[s_next].max()) - Q.loc[a,s])
            
            # The current state becomes the next state
            s = s_next
            
            # Add one to the discount exponent
            i += 1
            
        # append reward obtained during episode & forget episode reward
        Reward.append(episode_reward)
        episode_reward = 0
        i = 0
        
        if e % Eval_interval == 0:
            print(e)
            Optimal = Q.idxmax(axis=0)
            Forward_reward.append(Eval_policy(Coordinates, Actions, Gamma, Environment, Rewards, Q, Terminal, Eval_episodes, Initial_state, Optimal, P_slip))

    # Find optimal policy given estimated Q-values
    Optimal = Q.idxmax(axis=0)
    Optimal.loc[Terminal] = '-'
    Optimal = np.array(Optimal).reshape(Environment.shape)
    
    # Calculate average reward recursively    
    Average_reward = []
    Average_reward.append(Reward[0])
    for i in range(1, len(Reward)):
        Average_reward.append(Average_reward[-1] * i/(i+1) + Reward[i] * 1/(i+1))
 
    return Q, Reward, Optimal, Average_reward, Forward_reward

"""
Purpose:
    Implementation of SARSA
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Alpha: Learning rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    Episodes: Number of episodes to run algorithm
    Method: Exploration method
    
Outputs:
    Q: State-action values
    Cumulative_reward: list of cumulative rewards obtained for each episode 
    
Version:
    2019-05-01: First version
"""

def SARSA(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Environment, Rewards, P_slip, Episodes, Initial_state, Method, Eval_interval, Eval_episodes):
    
    # Initialize Q values & flatten rewards
    Q = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    
    # Determine terminal states
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]

    # Initialize non-terminal states
    for c in Coordinates:
        if c not in Terminal:
            Q[c] += Rewards.max().max()
    
    # Initialize cumulative reward & reward per episode        
    Reward = []
    Forward_reward = []
    episode_reward = 0
    i = 0

    for e in range(Episodes):
        
        # Initialize s (initial state) & episode reward
        s = Initial_state
        
        # select action based on Q and an exploration mechanism
        a = select_action(Actions, Epsilon, Tau, Q[s], Method)
        
        while s not in Terminal:
            
            # Take action a, and observe reward, r, and next state, s'
            (s_next, r) = simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip)
            
            # Add obtained reward to episode reward
            episode_reward += r * Gamma**i
            
            # Select action based on epsilon-greedy
            a_next = select_action(Actions, Epsilon, Tau, Q[s_next], Method)
            
            # Update Q
            Q.loc[a,s] += Alpha * (r + (Gamma * Q.loc[a_next,s_next]) - Q.loc[a,s])
            
            #The current state becomes the next state
            s = s_next
            a = a_next
            
            # Add one to discount exponent
            i += 1
            
        # Append reward obtained during episode & forget episode reward
        Reward.append(episode_reward)
        episode_reward = 0
        i = 0
            
        if e % Eval_interval == 0:
            print(e)
            Optimal = Q.idxmax(axis=0)
            Forward_reward.append(Eval_policy(Coordinates, Actions, Gamma, Environment, Rewards, Q, Terminal, Eval_episodes, Initial_state, Optimal, P_slip))

    
    # Find optimal policy given estimated Q-values
    Optimal = Q.idxmax(axis=0)
    Optimal.loc[Terminal] = '-'
    Optimal = np.array(Optimal).reshape(Environment.shape)
    
    # Calculate average reward recursively    
    Average_reward = []
    Average_reward.append(Reward[0])
    for i in range(1, len(Reward)):
        Average_reward.append(Average_reward[-1] * i/(i+1) + Reward[i] * 1/(i+1))
 
    return Q, Reward, Optimal, Average_reward, Forward_reward

# In[Q-learning Experience Replay]
 
"""
Purpose:
    Take random observations, update Q-function accordingly
    
Inputs:
    Q: Current Q-value estimate
    D: List of tuples with observations (s, a, s', r)
    N: Number of updates after each trajectory
    T: Length of each trajectory 
    l: Current trajectory count
    Alpha: Learning rate
    Gamme: Discount factor
    
Ouptuts:
    Q: Updates Q-estimate
    
Version:
    2019-05-03: First version
"""

def Q_learning_sampling(Q, D, N, T, l, Alpha, Gamma):
    
    # Get number of observations / experiences in D
    N_obs = len(D)
    
    # Update Q values
    for i in range(N*l*T):
        
        # Retrieve random sample
        idx = np.floor(rand.random() * N_obs)
        D_temp = D[int(idx)]
        
        # Update Q value
        Q.loc[D_temp[1],D_temp[0]] += Alpha * (D_temp[3] + (Gamma * Q[D_temp[2]].max()) - Q.loc[D_temp[1],D_temp[0]])
    
    return Q

"""
Purpose:
    Implementation of Q-Learning with experience replay
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Alpha: Learning rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    Episodes: Number of episodes to run algorithm
    Method: Exploration method
    T: Length of trajectory (number of steps before learning algorithm is applied)
    N: Number of updates after each trajectory
    
Outputs:
    Q: State-action values
    Reward: Reward per episode
    Optimal: Optimal deterministic policy for the agent
    Average_reward: Average reward from episode 1 to n
    
Version:
    2019-05-03: First version
"""

def Q_learning_ER(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Environment, Rewards, P_slip, Episodes, Initial_state, Method, T, N, Eval_interval, Eval_episodes):
    
    # Initialize Q values & flatten rewards
    Q = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    
    # Determine terminal states
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]

    # Initialize non-terminal states
    for c in Coordinates:
        if c not in Terminal:
            Q[c] += Rewards.max().max()
    
    # Initialize cumulative reward & reward per episode                
    Reward = []
    Forward_reward = []
    episode_reward = 0
    i = 0
    
    # Experience replay counters and database
    D = []
    l = 1
    k = 0
    
    for e in range(Episodes):
        
        # Initialize s (initial state)
        s = Initial_state
        
        while s not in Terminal:
            
            # Select action based on epsilon-greedy
            a = select_action(Actions, Epsilon, Tau, Q[s], Method)
            
            # Take action a, and observe reward, r, and next state, s'
            (s_next, r) = simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip)
            
            # Add obtained reward to episode reward
            episode_reward += r * Gamma**i
            
            # Calculate transition index, add sample to database
            D.append((s, a, s_next, r))
            k += 1
            
            # Update Q if trajectory complete
            if k == l*T:
                Q = Q_learning_sampling(Q, D, N, T, l, Alpha, Gamma)
                l += 1
                print("Completed trajectory " + str(l))

            # The current state becomes the next state
            s = s_next
            
            # Add one to the discount exponent
            i += 1
            
        # append reward obtained during episode & forget episode reward
        Reward.append(episode_reward)
        episode_reward = 0
        i = 0
        
        if e % Eval_interval == 0:
            print(e)
            Optimal = Q.idxmax(axis=0)
            Forward_reward.append(Eval_policy(Coordinates, Actions, Gamma, Environment, Rewards, Q, Terminal, Eval_episodes, Initial_state, Optimal, P_slip))

    # Find optimal policy given estimated Q-values
    Optimal = Q.idxmax(axis=0)
    Optimal.loc[Terminal] = '-'
    Optimal = np.array(Optimal).reshape(Environment.shape)
    
    # Calculate average reward recursively    
    Average_reward = []
    Average_reward.append(Reward[0])
    for i in range(1, len(Reward)):
        Average_reward.append(Average_reward[-1] * i/(i+1) + Reward[i] * 1/(i+1))
    
    return Q, Reward, Optimal, Average_reward, Forward_reward

# In[Eligibility traces]
    
"""
Notes to self:
    Algorithm runs until first exploritary action
    Lambda is a model parameter, meaning: ?
    Algorithm is not too different from standard Q-learning, can be used as a starting point
"""

"""
Purpose:
    Implementation of Q-Learning with eligibility traces
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Alpha: Learning rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    Episodes: Number of episodes to run algorithm
    Method: Exploration method
    
Outputs:
    Q: State-action values
    Optimal: Optimal deterministic policy for the agent
    Cumulative_reward: list of cumulative rewards obtained for each episode 
    
Version:
    2019-04-26: First version
"""

def Q_learning_ET(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Lambda, Environment, Rewards, P_slip, Episodes, Initial_state, Method, Eval_interval, Eval_episodes):
    
    # Initialize Q values
    Q = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    e = Q.copy()
    
    # Determine terminal states
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]

    # Initialize non-terminal states
    for c in Coordinates:
        if c not in Terminal:
            Q[c] += Rewards.max().max()
    
    # Initialize cumulative reward & reward per episode                
    Forward_reward = []
    Reward = []
    episode_reward = 0
    i = 0

    for episode in range(Episodes):
        
        # Initialize s (initial state) and a (first action)
        s = Initial_state
        a = select_action(Actions, Epsilon, Tau, Q[s], Method)
        
        while s not in Terminal:
                        
            # Take action a, and observe reward, r, and next state, s'
            (s_next, r) = simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip)
            
            # Add obtained reward to episode reward
            episode_reward += r * Gamma**i
            
            # Simulate next action and next optimal action, set equal if same Q-value
            a_next = select_action(Actions, Epsilon, Tau, Q[s_next], Method)
            a_opt = Q[s_next].idxmax()
            
            if a_next != a_opt and Q.loc[a_next,s_next] == Q.loc[a_opt,s_next]:
                a_opt = a_next
            
            # Calculate TD error and enumerate e (number of steps counter)
            delta = r + Gamma * Q.loc[a_opt, s_next] - Q.loc[a, s]
            e.loc[a, s] += 1
            
            # Update every Q-value based on #times visited
            for aTemp in range(len(Actions)):
                
                for sTemp in e.iloc[aTemp].nonzero():
                    
                    # Update Q-value
                    Q.iloc[aTemp, sTemp] += Alpha * delta * e.iloc[aTemp, sTemp]
                    
                    # Adjust visit count
                    if a_next == a_opt:
                        e.iloc[aTemp, sTemp] = Gamma * Lambda * e.iloc[aTemp, sTemp]
                    else:
                        e.iloc[aTemp, sTemp] = 0
            
            # The current state becomes the next state
            s = s_next
            a = a_next
            
            # Add one to the discount exponent
            i += 1
            
        # append reward obtained during episode & forget episode reward
        Reward.append(episode_reward)
        episode_reward = 0
        i = 0
        
        if episode % Eval_interval == 0:
            print(episode)
            Optimal = Q.idxmax(axis=0)
            Forward_reward.append(Eval_policy(Coordinates, Actions, Gamma, Environment, Rewards, Q, Terminal, Eval_episodes, Initial_state, Optimal, P_slip))

    # Find optimal policy given estimated Q-values
    Optimal = Q.idxmax(axis=0)
    Optimal.loc[Terminal] = '-'
    Optimal = np.array(Optimal).reshape(Environment.shape)
    
    # Calculate average reward recursively    
    Average_reward = []
    Average_reward.append(Reward[0])
    for i in range(1, len(Reward)):
        Average_reward.append(Average_reward[-1] * i/(i+1) + Reward[i] * 1/(i+1))
 
    return Q, Reward, Optimal, Average_reward, Forward_reward

# In[Prioritized sweeping]

"""
Purpose:
    Implementation of Prioritized Sweeping
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Alpha: Learning rate
    Epsilon: Parameter for epsilon greedy exploration
    Tau: Parameter for softmax exploration
    Lambda: Importance parameter for prioritized sweeping
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    Episodes: Number of episodes to run algorithm
    Method: Exploration method
    N: Maximum number of updates at each timestep
    
Outputs:
    Q: State-action values
    Optimal: Optimal deterministic policy for the agent
    Cumulative_reward: list of cumulative rewards obtained for each episode 
    
Version:
    2019-05-10: First version
"""

def Prioritized_sweeping(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Lambda, Environment, Rewards, P_slip, Episodes, Initial_state, Method, N, Eval_interval, Eval_episodes):
    
    # Initialize Q values & flatten rewards
    Q = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    
    # Determine terminal states
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]

    # Initialize non-terminal states
    for c in Coordinates:
        if c not in Terminal:
            Q[c] += Rewards.max().max()
    
    # Initialize model
    Model = {}
    for s in Coordinates:
        for a in Actions:
            Model[s,a] = [0.0, 0]
            
    # Initialize PQueue
    PQueue = []
    
    # Initialize cumulative reward & reward per episode                
    Forward_reward = []
    Reward = []
    episode_reward = 0
    i = 0

    # Initialize model
    N_model = pd.DataFrame(np.zeros([len(Coordinates), len(Coordinates) * len(Actions)]), index = Coordinates, columns = pd.MultiIndex.from_product([Coordinates, Actions]))
    T_model = N_model.copy()
    R_model = N_model.copy()
    
#    # Make transition probability matrix
#    T = pd.DataFrame(index = Coordinates, columns = pd.MultiIndex.from_product([Coordinates, Actions]))
#    for s in Coordinates:
#        for a in Actions:
#            T[s,a] = perform_action(Environment, s, a, Actions, P_slip).flatten()
    
    for e in range(Episodes):
        
        s = Initial_state
        
        while s not in Terminal:
            
            # Select action based on epsilon-greedy
            a = select_action(Actions, Epsilon, Tau, Q[s], Method)
            
            # Take action a, and observe reward, r, and next state, s'
            (s_next, r) = simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip)
            
            # Store outcomes in Model
            N_model[s,a][s_next] += 1
            T_model[s,a] = N_model[s,a] / N_model[s,a].sum()
            R_model[s,a][s_next] = r
            
            # Add obtained reward to episode reward
            episode_reward += r * Gamma**i
    
            # Calculate importance, add to list if important enough
            p = np.abs(r + Gamma * Q[s_next].max() - Q.loc[a,s])
            if p >= Lambda:
                PQueue.append((s, a, p))
                PQueue.sort(key=lambda tup: tup[2], reverse=True)
            
            n = 0
            while PQueue != [] and n < N:
                
                # Retrieve state information
                sTemp, aTemp, _ = PQueue[0]
                s_nextTemp = T_model[sTemp, aTemp]
                rTemp = R_model[sTemp, aTemp]

                # Drop current state information to avoid double entries
                PQueue = [item for item in PQueue if item[0] != sTemp or item[1] != aTemp]

                # Update Q
                Q.loc[aTemp, sTemp] += Alpha * (np.dot(rTemp, s_nextTemp) + Gamma * np.dot(Q.max(), s_nextTemp) - Q.loc[aTemp, sTemp])

                # Add all states leading to this to PQueue
                Arrivals = T_model.loc[sTemp,:].nonzero()[0]
                for arr in Arrivals:
                    s_bar = T_model.iloc[:,arr].name[0]
                    a_bar = T_model.iloc[:,arr].name[1]
                    r_bar = R_model[s_bar, a_bar]
                    p = np.abs(np.dot(r_bar, T_model[s_bar, a_bar]) + Gamma * np.dot(Q.max(), T_model[s_bar, a_bar]) - Q.loc[a_bar, s_bar])
                    if p >= Lambda:
                        PQueue.append((s_bar, a_bar, p))
                        
                # Remove previous entry, duplicates and sort
                PQueue = list(dict.fromkeys(PQueue))
                PQueue.sort(key=lambda tup: tup[2], reverse=True)
                
                n += 1
                
            # The current state becomes the next state
            s = s_next
            
            # Add one to the discount exponent
            i += 1
            
        # append reward obtained during episode & forget episode reward
        Reward.append(episode_reward)
        episode_reward = 0
        i = 0
        
        if e % Eval_interval == 0:
            print(e)
            Optimal = Q.idxmax(axis=0)
            Forward_reward.append(Eval_policy(Coordinates, Actions, Gamma, Environment, Rewards, Q, Terminal, Eval_episodes, Initial_state, Optimal, P_slip))

    # Find optimal policy given estimated Q-values
    Optimal = Q.idxmax(axis=0)
    Optimal.loc[Terminal] = '-'
    Optimal = np.array(Optimal).reshape(Environment.shape)
    print(Optimal)   
    
    # Calculate average reward recursively    
    Average_reward = []
    Average_reward.append(Reward[0])
    for i in range(1, len(Reward)):
        Average_reward.append(Average_reward[-1] * i/(i+1) + Reward[i] * 1/(i+1))
 
    return Q, Reward, Optimal, Average_reward, Forward_reward
    
# In[On-policy Monte-Carlo]
   
"""
Purpose:
    Implementation of On-policy Monte Carlo
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Alpha: Learning rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    Episodes: Number of episodes to run algorithm
    Method: Exploration method
    
Outputs:
    Q: State-action values
    Optimal: Optimal deterministic policy for the agent
    Cumulative_reward: list of cumulative rewards obtained for each episode 
    
Version:
    2019-05-14: First version
"""

def OnP_MC(Coordinates, Actions, Gamma, Epsilon, Tau, Environment, Rewards, P_slip, Episodes, Initial_state, Method, Eval_interval, Eval_episodes):
    
    # Initialize Q values & flatten rewards
    Q = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)

    # Determine terminal states
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]

    # Initialize non-terminal states
    for c in Coordinates:
        if c not in Terminal:
            Q[c] += Rewards.max().max()

    # Make returns dictionary
    R = {}
    for s in Coordinates:
        for a in Actions:
            R[s,a] = [Q.loc[a,s], 1]
            
    # Initialize cumulative reward & reward per episode                
    Forward_reward = []
    Reward = []
    episode_reward = 0
    i = 0

    for e in range(Episodes):
        
        # Initialize path information
        Qpath = []
        Rpath = []
        
        # Initialize s (initial state)
        s = Initial_state
        
        while s not in Terminal:
            
            # Select action based on epsilon-greedy
            a = select_action(Actions, Epsilon, Tau, Q[s], Method)
            
            # Take action a, and observe reward, r, and next state, s'
            (s_next, r) = simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip)
            
            # Add obtained reward to episode reward
            episode_reward += r * Gamma**i

            # Add information of states
            Qpath.append((s,a))
            Rpath.append(r)
            
            # The current state becomes the next state
            s = s_next
            
            # Add one to the discount exponent
            i += 1
            
        # Calculate discounted rewards per time step (backwards)
        RewardPath = [Rpath[-1]]
        for i in range(len(Rpath)-2, -1, -1):
            RewardPath.insert(0, Gamma * RewardPath[0] + Rpath[i])
        
        # Retrieve unique states that are visited
        Q_unique = list(set(Qpath))
        for StateAction in Q_unique:
            
            # Retrieve state, index and index in list
            StateTemp = StateAction[0]
            ActionTemp = StateAction[1]
            idx = Qpath.index(StateAction)
            
            # Retrieve old Q estimate and number of times visited
            Q_old = R[StateTemp, ActionTemp]
            CountOld = Q_old[1]
            
            # Calculate new Q-estimate
            Q_new = (Q_old[0] * CountOld / (CountOld + 1) + RewardPath[idx] / (CountOld + 1))
            R[StateAction[0],StateAction[1]] = [Q_new, CountOld + 1]
            Q.loc[ActionTemp, StateTemp] = Q_new

        # Append reward obtained during episode & forget episode reward
        Reward.append(episode_reward)
        episode_reward = 0
        i = 0
        
        if e % Eval_interval == 0:
            print(e)
            Optimal = Q.idxmax(axis=0)
            Forward_reward.append(Eval_policy(Coordinates, Actions, Gamma, Environment, Rewards, Q, Terminal, Eval_episodes, Initial_state, Optimal, P_slip))

    # Find optimal policy given estimated Q-values
    Optimal = Q.idxmax(axis=0)
    Optimal.loc[Terminal] = '-'
    Optimal = np.array(Optimal).reshape(Environment.shape)
    
    # Calculate average reward recursively    
    Average_reward = []
    Average_reward.append(Reward[0])
    for i in range(1, len(Reward)):
        Average_reward.append(Average_reward[-1] * i/(i+1) + Reward[i] * 1/(i+1))
 
    return Q, Reward, Optimal, Average_reward, Forward_reward

# In[Double Q-learning]

"""
Purpose:
    Implementation of Double Q-Learning. Selects action based on regular 
    exploration of the update matrix. Updates in alternating fashion
    
Inputs:
    Coordinates: Tuple with all the coordinates the agent can reach
    Actions: Tuple with all actions the agent can take
    Gamma: Discount rate
    Alpha: Learning rate
    Environment = Textual representation of the environment as made in create_environment
    Rewards: Dataframe with rewards
    P_slip: Probability of slipping and sliding to last state in chosen direction
    Episodes: Number of episodes to run algorithm
    Method: Exploration method
    
Outputs:
    Q: State-action values
    Optimal: Optimal deterministic policy for the agent
    Cumulative_reward: list of cumulative rewards obtained for each episode 
    
Version:
    2019-05-14: First version
"""

def Double_Q_learning(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Environment, Rewards, P_slip, Episodes, Initial_state, Method, Eval_interval, Eval_episodes):
    
    # Initialize Q values & flatten rewards
    Qa = pd.DataFrame(np.zeros([len(Actions),len(Coordinates)]), index=Actions, columns=Coordinates)
    
    # Determine terminal states
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')]

    # Initialize non-terminal states
    for c in Coordinates:
        if c not in Terminal:
            Qa[c] += Rewards.max().max()
    
    # Create second Q-value matrix and alternating variable
    Selected = 0
    Qb = Qa.copy()
    
    # Initialize cumulative reward & reward per episode                
    Forward_reward = []
    Reward = []
    episode_reward = 0
    i = 0

    for e in range(Episodes):
        
        # Initialize s (initial state)
        s = Initial_state
        
        while s not in Terminal:
            
            # Select action based on epsilon-greedy
            if Selected == 0:
                a = select_action(Actions, Epsilon, Tau, Qa[s], Method)
            else:
                a = select_action(Actions, Epsilon, Tau, Qb[s], Method)
            
            # Take action a, and observe reward, r, and next state, s'
            (s_next, r) = simulate_action(Environment, Rewards, Coordinates, s, a, Actions, P_slip)
            
            # Add obtained reward to episode reward
            episode_reward += r * Gamma**i
            
            # Find next optimal action, update other Q
            if Selected == 0:
                a_next = Qa[s_next].idxmax()
                Qa.loc[a,s] += Alpha * (r + (Gamma * Qb.loc[a_next, s_next]) - Qa.loc[a,s])
            else:
                a_next = Qb[s_next].idxmax()
                Qb.loc[a,s] += Alpha * (r + (Gamma * Qa.loc[a_next, s_next]) - Qb.loc[a,s])
            
            # The current state becomes the next state
            s = s_next
            
            # Add one to the discount exponent
            i += 1
            
            # Switch update and target
            Selected = (1 - Selected)
            
        # append reward obtained during episode & forget episode reward
        Reward.append(episode_reward)
        episode_reward = 0
        i = 0
        
        if e % Eval_interval == 0:
            print(e)
            Optimal = Qa.idxmax(axis=0)
            Forward_reward.append(Eval_policy(Coordinates, Actions, Gamma, Environment, Rewards, Qa, Terminal, Eval_episodes, Initial_state, Optimal, P_slip))

    # Find optimal policy given estimated Q-values
    Optimal_a = Qa.idxmax(axis=0)
    Optimal_a.loc[Terminal] = '-'
    Optimal_a = np.array(Optimal_a).reshape(Environment.shape)
    
    Optimal_b = Qb.idxmax(axis=0)
    Optimal_b.loc[Terminal] = '-'
    Optimal_b = np.array(Optimal_b).reshape(Environment.shape)
    
    # Calculate average reward recursively    
    Average_reward = []
    Average_reward.append(Reward[0])
    for i in range(1, len(Reward)):
        Average_reward.append(Average_reward[-1] * i/(i+1) + Reward[i] * 1/(i+1))
 
    return Qa, Qb, Reward, Optimal_a, Optimal_b, Average_reward, Forward_reward  

# In[Plot functions]

"""
Purpose:
    Plot average reward over episodes
    
Inputs:
    Average_reward: Average reward from start to that time period
    Title: Name of the algorithm used to collect these rewards
    
Version:
    2019-05-03: First version
"""

#def Average_reward_plot(Average_reward1, Average_reward2 , Title, label1, label2):
#    
#    plt.plot(Average_reward1, label = label1)
#    plt.plot(Average_reward2, label = label2)
#    plt.legend(loc = 'lower right')
#
#    plt.xlabel("Episode")
#    plt.ylabel("Average discounted cumulative reward")
#    plt.title("Online performace - \n" + Title)
#    plt.savefig("Average_reward_"+ Title + ".png")
#    plt.show()
#    
#    return

"""
Purpose:
    Plot average reward over episodes
    
Inputs:
    list_data: List that contains avarage rewards
    list_names: 
    
Version:
    2019-05-03: First version
"""

def Average_reward_plot(list_data, list_names, title, save = False):
    
    for i in range(len(list_data)):
        plt.plot(list_data[i], label = list_names[i])

    plt.legend(loc = 'lower right')
    plt.xlabel("Episode")
    plt.ylabel("Average cumulative discounted reward")
    plt.title("Online performace \n" + title)
    if(save):
        plt.savefig("Average_reward_"+ title + ".png")
    plt.show()
    
    return

def Forward_reward_plot(list_data,list_names, title, Eval_interval, save = False):
    
    length = len(list_data[0])
    x = [element * Eval_interval for element in list(range(1, length+1))]
    
    for i in range(len(list_data)):
        plt.plot(x, list_data[i], label = list_names[i])

    plt.legend(loc = 'lower right')
    plt.xlabel("Episode")
    plt.ylabel("Average cumulative discounted reward")
    plt.title("Policy performace \n" + title)
    if(save):
        plt.savefig("Average_reward_"+ title + ".png")
    plt.show()
    
    return

# In[Main]

def main():
    
    ##### Creating environment #####
    Environment, Rewards = create_environment(N_rows, N_cols, Env_end, Env_wreck, Env_crack, Reward_end, Reward_wreck, Reward_crack)
    
    # Retrieve all coordinates of the grid
    Coordinates = ()
    for i in range(N_rows):
        for j in range(N_cols):
            Coordinates = (*Coordinates, (i,j))
    
    # Determine terminal states
    Terminal = [c for c in Coordinates if Environment.iloc[c] in ('C', 'E')] #analysis:ignore
    
    # Get baseline Q-values from Value Iteration
    V_VI, Q_VI, Optimal_VI = value_iteration(Coordinates, Actions, Gamma, Environment, Rewards, P_slip)
    
    ##### Q8: Q-learning - Epsilon greedy #####
    rand.seed(42)
    N_episodes = 3000
    Eval_interval = 50
    Eval_episodes = 500
    Method = "Epsilon_greedy"
    Q_QL_Epsilon, Reward_QL_Epsilon, Optimal_QL_Epsilon, Average_reward_QL_Epsilon, Forward_reward_QL_Epsilon = Q_learning(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau,  Environment, Rewards, P_slip, N_episodes, Initial_state, Method, Eval_interval, Eval_episodes)
    print(Optimal_QL_Epsilon)
#    Average_reward_plot([Average_reward_QL_Epsilon], ["Q-learning Epsilon Greedy"] , "Q-learning Epsilon Greedy")
    
    ##### Q9: Q-learning - Softmax #####
    rand.seed(42)
    N_episodes = 3000
    Method = "Softmax"
    Q_QL_Softmax, Reward_QL_Softmax, Optimal_QL_Softmax, Average_reward_QL_Softmax, Forward_reward_QL_Softmax = Q_learning(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau,  Environment, Rewards, P_slip, N_episodes, Initial_state, Method, Eval_interval, Eval_episodes)
    print(Optimal_QL_Softmax)
#    Average_reward_plot([Average_reward_QL_Softmax], ["Q-learning Softmax"],"Q-learning Softmax" )
    Forward_reward_plot([Forward_reward_QL_Epsilon,Forward_reward_QL_Softmax], ["Epsilon greedy","Softmax"],"Q-learning - Epsilon greedy / Softmax",Eval_interval )
    
    ##### Q10: SARSA #####
    rand.seed(42)
    N_episodes = 3000
    Method = "Softmax"
    Q_SARSA, Reward_SARSA, Optimal_SARSA, Average_reward_SARSA, Forward_reward_SARSA = SARSA(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau,  Environment, Rewards, P_slip, N_episodes, Initial_state, Method, Eval_interval, Eval_episodes) #analysis:ignore
    print(Optimal_SARSA)
#    Average_reward_plot([Average_reward_SARSA], ["SARSA"], "SARSA")
    
    Average_reward_plot([Average_reward_QL_Softmax[0:500],Average_reward_SARSA[0:500]], ["Q-learning Softmax","SARSA"], "Q-Learning / SARSA (Softmax)")
    Forward_reward_plot([Forward_reward_QL_Softmax, Forward_reward_SARSA], ["Q-learning Softmax","SARSA"], "Q-Learning / SARSA (Softmax)", Eval_interval) 

    ##### Q11: Experience replay #####
    rand.seed(42)
    Method = "Softmax"
    N_episodes = 500
    Eval_interval = 10
    Eval_episodes = 500
    Q_ER, Reward_ER, Optimal_ER, Average_reward_ER, Forward_reward_ER = Q_learning_ER(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Environment, Rewards, P_slip, N_episodes, Initial_state, Method, T_ER, N_ER, Eval_interval, Eval_episodes)
    print(Optimal_ER)
#    Average_reward_plot([Average_reward_ER], ["Experience Replay"], "Experience Replay")
#    Forward_reward_plot([Forward_reward_ER], ["Experience Replay"], "Experience Replay", Eval_interval)
    
    ##### Q12: Eligibility Traces #####
    rand.seed(42)
    Method = "Softmax"
    N_episodes = 500
    Eval_interval = 10
    Eval_episodes = 500
    Q_ET, Reward_ET, Optimal_ET, Average_reward_ET, Forward_reward_ET = Q_learning_ET(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Lambda, Environment, Rewards, P_slip, N_episodes, Initial_state, Method, Eval_interval, Eval_episodes)
    print(Optimal_ET)
#    Average_reward_plot([Average_reward_ET], ["Eligibility Traces"],"Eligibility Traces")
#    Forward_reward_plot([Forward_reward_ER], ["Experience Replay"], "Experience Replay", Eval_interval)

    ##### Q14a: Prioritized Sweeping #####
    rand.seed(42)
    Method = "Epsilon_greedy"
    N_episodes = 500
    Eval_interval = 10
    Eval_episodes = 500
    Q_PS, Reward_PS, Optimal_PS, Average_reward_PS, Forward_reward_PS = Prioritized_sweeping(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau, Lambda, Environment, Rewards, P_slip, N_episodes, Initial_state, Method, N_PS, Eval_interval, Eval_episodes)
    print(Optimal_PS)
#    Average_reward_plot([Average_reward_PS], ["Prioritized Sweeping"],"Prioritized Sweeping")


    Forward_reward_plot([Forward_reward_ER,Forward_reward_ET,Forward_reward_PS], ["Experience Replay","Eligibility Traces","Prioritized Sweeping"], "Experience Replay / Eligibility Traces / Prioritized Sweeping", Eval_interval)

    ##### Q14b: On-policy Monte Carlo #####
    rand.seed(42)
    Method = "Epsilon_greedy"
    N_episodes = 3000
    Eval_interval = 50
    Eval_episodes = 500
    Q_MC, Reward_MC, Optimal_MC, Average_reward_MC, Forward_reward_MC = OnP_MC(Coordinates, Actions, Gamma, Epsilon, Tau, Environment, Rewards, P_slip, N_episodes, Initial_state, Method, Eval_interval, Eval_episodes)
    print(Optimal_MC)
#    Average_reward_plot([Average_reward_MC], ["On-policy Monte Carlo"],"On-policy Monte Carlo")

    ##### Q14c: Double Q-learning #####
    rand.seed(42)
    Method = "Softmax"
    N_episodes = 3000
    Eval_interval = 50
    Eval_episodes = 500
    Qa, Qb, Reward_double, Optimal_a, Optimal_b, Average_reward_double, Forward_reward_double = Double_Q_learning(Coordinates, Actions, Gamma, Alpha, Epsilon, Tau,  Environment, Rewards, P_slip, N_episodes, Initial_state, Method, Eval_interval, Eval_episodes)
    print(Optimal_a)
    print(Optimal_b)    
#    Average_reward_plot([Average_reward_double], ["Double Q-learning"],"Double Q-learning")
    
    Forward_reward_plot([Forward_reward_MC, Forward_reward_double], ["On-policy Monte Carlo", "Double Q-learning"], "Monte Carlo / Double Q-learning", Eval_interval)
    
### start main
if __name__ == "__main__":
    main()

# In[]
##### Q14b: On-policy Monte Carlo #####
rand.seed(42)
Method = "Epsilon_greedy"
N_episodes = 50000
Epsilon = 0.1
Eval_interval = 100
Eval_episodes = 250
Q_MC, Reward_MC, Optimal_MC, Average_reward_MC, Forward_reward_MC = OnP_MC(Coordinates, Actions, Gamma, Epsilon, Tau, Environment, Rewards, P_slip, N_episodes, Initial_state, Method, Eval_interval, Eval_episodes)
print(Optimal_MC)
Forward_reward_plot([Forward_reward_MC], ["On-policy Monte Carlo"], "On-policy Monte-Carlo", Eval_interval)
#    Average_reward_plot([Average_reward_MC], ["On-policy Monte Carlo"],"On-policy Monte Carlo")
